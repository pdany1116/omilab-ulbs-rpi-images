# RPi Image of the mBot
Pre-configured image of the RPi for mBot with Tomcat webserver.

## Configuration parameter

- Static IP: 10.14.10.251
- User: pi
- Password: ATU39218002_pi
- Shrinked file system (expand after copy to SD)
- OMiLAB ULBS wifi credentials added
- Keyboard Language: US

## Setup Ubuntu
1. Enter directory
```bash
cd omilab-ulbs-rpi-images/mBot
```
2. Extract the compressed image file using Gzip:
```bash
gunzip mBot.img.gz
```
Creates a new file `mBot.img`

3. Insert an empty SD card (32GB)
   
4. Check the SD Card device names
```bash
sudo fdisk -l
```
Example output:

![image](https://github.com/pdany1116/omilab-ulbs-rpi-images/assets/51260103/edd54d3b-3651-4ac7-aa01-666063b77368)

5. Unmount the device
```bash
sudo umount /dev/sda1 /dev/sda2
```
6. Clone the image on the SD Card
```bash
sudo dd if=./mBot.img of=/dev/sda
```
This might take a while. Do not interrupt the process.
7. Insert the SD Card in Raspberry Pi. You can access the mBot webserver at: http://10.14.10.251:8080/mBot

### Optional
1. Add a new wifi network
```bash
cd /<user>/media/root
sudo nano etc/wpa_supplicant/wpa_supplicant.conf
```
Paste the following at the end of file and replace with your credentials:
```
network={
    ssid="<ssid>"
    psk="<password>"
}
```
Exit and save.

2. Set a new static IP Address
```bash
cd /<user>/media/root
sudo nano etc/dhcpcd.conf
```
Find and edit the following line:
```
interface wlan0
static ip_address=10.14.10.251/24
```

## Create new image from exisint SD Card
1. Clone image
```bash
sudo dd if=/dev/sda of=~/mBot.img
```
2. Compress image
```bash
gzip mBot.img
```